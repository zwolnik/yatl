import React from "react"
import styled from "styled-components"

const StyledFooter = styled.footer`
  background-color: #313131;
  text-align: right;
  font-size: 1em;
  padding: 0.1em;
  width: 100%;
  position: fixed;
  bottom: 0;
`

const FooterText = styled.span`
  font-style: italic;
  color: #ffffff;
  padding-right: 1em;
`

type Props = {
  text: string
}

const Footer = (props: Props): React.ReactElement => {
  return (
    <StyledFooter>
      <FooterText>{props.text}</FooterText>
    </StyledFooter>
  )
}

export { Footer }
