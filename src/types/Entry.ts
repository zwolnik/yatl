import { TodoStatus } from "./TodoStatus"

export type Entry = {
  status: TodoStatus
  content: string
  created: Date
}
