import React, { useState } from "react"
import styled from "styled-components"

const AddTodoInput = styled.input`
  type: text;
  border: solid;
  border-color: #313131;
`

const AddTodoButton = styled.input`
  type: submit;
  background-color: none;
  border: solid;
  color: #313131;
  text-decoration: none;
  margin: 4px 2px;
  cursor: pointer;
`

type Props = {
  onSubmit: (value: string) => void
}

const AddTodo = (props: Props): React.ReactElement => {
  const [value, setValue] = useState("")

  const onSubmit = (event: React.FormEvent): void => {
    event.preventDefault()
    props.onSubmit(value)
    setValue("")
  }

  return (
    <form onSubmit={onSubmit}>
      <AddTodoInput
        placeholder="a thing to do"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <AddTodoButton type="submit" value="add" />
    </form>
  )
}

export { AddTodo }
