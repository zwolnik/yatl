import React from "react"
import { fireEvent, render } from "@testing-library/react"
import { AddTodo } from "components/AddTodo"

test("should invoke callback once", () => {
  const callback = jest.fn()
  const { getByRole } = render(<AddTodo onSubmit={callback} />)
  const input = getByRole("textbox")
  const submit = getByRole("button")

  fireEvent.change(input, { target: { value: "user input" } })
  fireEvent.click(submit)

  expect(callback).toBeCalledTimes(1)
})

test("should invoke callback with passed input", () => {
  const callback = jest.fn()
  const { getByRole } = render(<AddTodo onSubmit={callback} />)
  const input = getByRole("textbox")
  const submit = getByRole("button")

  fireEvent.change(input, { target: { value: "user input" } })
  fireEvent.click(submit)

  expect(callback).toBeCalledWith("user input")
})
