import React from "react"
import { fireEvent, render } from "@testing-library/react"
import App from "App"

describe("<App />", () => {
  xit("adds new elements with todo state", () => {
    const { getByPlaceholderText, getByText } = render(<App />)
    const input = getByPlaceholderText("a thing to do")
    const submit = getByText("add")

    fireEvent.change(input, { target: { value: "foo" } })
    fireEvent.click(submit)

    expect(getByText("[ ]")).toBeInTheDocument()
    expect(getByText("foo")).toBeInTheDocument()
  })

  xit("allows for changing status of item", () => {
    const { getByPlaceholderText, getByText } = render(<App />)
    const input = getByPlaceholderText("a thing to do")
    const submit = getByText("add")

    fireEvent.change(input, { target: { value: "foo" } })
    fireEvent.click(submit)

    const moveToStarted = getByText("started")
    fireEvent.click(moveToStarted)

    expect(getByText("[.]")).toBeInTheDocument()
    expect(getByText("foo")).toBeInTheDocument()
  })
})
