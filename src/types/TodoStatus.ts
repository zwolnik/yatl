export enum TodoStatus {
  Todo = "Todo",
  Started = "Started",
  Done = "Done",
  Obsolete = "Obsolete",
}
