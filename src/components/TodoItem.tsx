import React from "react"
import styled from "styled-components"

const Prefix = styled.pre<{ status: TodoStatus }>`
  display: inline;
  font-size: 1.5em;

  color: ${(props) => {
    if (props.status === "Todo") return "black"
    else if (props.status === "Started") return "green"
    else if (props.status === "Done") return "darkgray"
    else return "gray"
  }}};

  &::before {
    content: ${(props) => {
      if (props.status === "Todo") return '"[ ]"'
      else if (props.status === "Started") return '"[.]"'
      else if (props.status === "Done") return '"[x]"'
      else return '"[-]"'
    }}};
  }
`

const Description = styled.span<{ status: TodoStatus }>`
  display: inline;
  margin-left: 1em;
  margin-right: 1em;
  text-align: left;

  color: ${(props) => {
    if (props.status === "Done") return "darkgray"
    else if (props.status === "Obsolete") return "gray"
    else return "black"
  }}};

  text-decoration: ${(props) => {
    if (props.status === "Started") return "underline"
    else if (props.status === "Done") return "line-through"
    else return "none"
  }}};
`

import { TodoStatus } from "types/TodoStatus"

type Props = {
  status: TodoStatus
  desc: string
}

const TodoItem = (props: Props): React.ReactElement => {
  return (
    <>
      <Prefix status={props.status} />
      <Description status={props.status}>{props.desc}</Description>
    </>
  )
}

export { TodoItem }
