import React from "react"
import { render } from "@testing-library/react"
import { Header } from "components/Header"

it("renders all words passed to header", () => {
  const { getByText } = render(<Header text={"Fancy Header"} />)
  expect(getByText("Fancy")).toBeInTheDocument()
  expect(getByText("Header")).toBeInTheDocument()
})
