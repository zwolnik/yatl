import React, { useState } from "react"
import styled from "styled-components"

import { AddTodo } from "components/AddTodo"
import { Footer } from "components/Footer"
import { Header } from "components/Header"
import { TodoList } from "components/TodoList"
import { TodoStatus } from "types/TodoStatus"

type ItemSetter = React.Dispatch<React.SetStateAction<string[]>>

const createMoveFn =
  (from: string[], setFrom: ItemSetter, to: string[], setTo: ItemSetter) =>
  (value: string) => {
    const newFrom = from
    const indexToDelete = newFrom.indexOf(value)
    newFrom.splice(indexToDelete, 1)
    setFrom(newFrom)
    setTo([value, ...to])
  }

const StyledMain = styled.main`
  margin: 2em;
`

function App(): React.ReactElement {
  const [todo, setTodo] = useState<string[]>([])
  const [started, setStarted] = useState<string[]>([])
  const [done, setDone] = useState<string[]>([])
  const [obsolete, setObsolete] = useState<string[]>([])

  const addTodo = (value: string) => {
    setTodo([value, ...todo])
  }

  const todoMoveFn = (to: string[], setTo: ItemSetter) =>
    createMoveFn(todo, setTodo, to, setTo)
  const todoMoveFns = {
    started: todoMoveFn(started, setStarted),
    done: todoMoveFn(done, setDone),
    obsolete: todoMoveFn(obsolete, setObsolete),
  }

  const startedMoveFn = (to: string[], setTo: ItemSetter) =>
    createMoveFn(started, setStarted, to, setTo)
  const startedMoveFns = {
    todo: startedMoveFn(todo, setTodo),
    done: startedMoveFn(done, setDone),
    obsolete: startedMoveFn(obsolete, setObsolete),
  }

  const doneMoveFn = (to: string[], setTo: ItemSetter) =>
    createMoveFn(done, setDone, to, setTo)
  const doneMoveFns = {
    todo: doneMoveFn(todo, setTodo),
    started: doneMoveFn(started, setStarted),
    obsolete: doneMoveFn(obsolete, setObsolete),
  }

  const obsoleteMoveFn = (to: string[], setTo: ItemSetter) =>
    createMoveFn(obsolete, setObsolete, to, setTo)
  const obsoleteMoveFns = {
    todo: obsoleteMoveFn(todo, setTodo),
    started: obsoleteMoveFn(started, setStarted),
    done: obsoleteMoveFn(done, setDone),
  }

  return (
    <div className="App">
      <Header text="Yet Another Todo List" />
      <StyledMain>
        <AddTodo onSubmit={addTodo} />
        <TodoList status={TodoStatus.Todo} items={todo} moveFns={todoMoveFns} />
        <TodoList
          status={TodoStatus.Started}
          items={started}
          moveFns={startedMoveFns}
        />
        <TodoList status={TodoStatus.Done} items={done} moveFns={doneMoveFns} />
        <TodoList
          status={TodoStatus.Obsolete}
          items={obsolete}
          moveFns={obsoleteMoveFns}
        />
      </StyledMain>
      <Footer text="(setq nothing t everything 'permitted)" />
    </div>
  )
}

export default App
