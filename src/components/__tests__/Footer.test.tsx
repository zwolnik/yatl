import React from "react"
import { render } from "@testing-library/react"
import { Footer } from "components/Footer"

test("should render passed text", () => {
  const { getByText } = render(<Footer text="Fancy Footer" />)
  expect(getByText("Fancy Footer")).toBeInTheDocument()
})
