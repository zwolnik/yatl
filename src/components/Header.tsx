import React from "react"
import styled from "styled-components"

const BoldFirst = styled.span`
  display: inline-block;
  margin-right: 0.2em;

  &::first-letter {
    font-weight: bold;
  }

  &:last-child {
    margin-right: 0;
  }
}
`

const StyledHeader = styled.header`
  color: #ffffff;
  background-color: #313131;
  text-align: center;
  font-size: 1.5em;
  padding: 0.3em;
`

type Props = {
  text: string
}

const Header = (props: Props): React.ReactElement => {
  return (
    <StyledHeader>
      {props.text.split(" ").map((word) => (
        <BoldFirst key={word}>{word}</BoldFirst>
      ))}
    </StyledHeader>
  )
}

export { Header }
