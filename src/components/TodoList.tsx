import React from "react"
import styled from "styled-components"

import { TodoItem } from "components/TodoItem"
import { TodoStatus } from "types/TodoStatus"

const Button = styled.button`
  border: none;
  color: #ffffff;
  background-color: #212121;
  margin: none;
  margin-right: 0.2em;
`

type Props = {
  status: TodoStatus
  items: Array<string>
  moveFns: Record<string, (value: string) => void>
}

const TodoList = (props: Props): React.ReactElement => {
  return (
    <div>
      <h3>{props.status}</h3>
      {props.items.map((value) => (
        <div key={props.status + value}>
          <TodoItem status={props.status} desc={value} />
          {Object.entries(props.moveFns).map(([key, fn]) => (
            <Button
              key={props.status + key + value + "-button"}
              type="button"
              onClick={() => fn(value)}
            >
              {key}
            </Button>
          ))}
        </div>
      ))}
    </div>
  )
}

export { TodoList }
